package com.DAO;

import com.Family;
import com.Human;
import com.Pet;

import java.util.List;

public interface FamilyDao {
     Family getFamilyByIndex(int index);

    List getAllFamilies();

       void displayAllFamilies();
      void getFamiliesBiggerThan(int number);
      void getFamiliesLessThan(int number);
      void createNewFamily(Human human, Human human2);
    boolean deleteFamily(int index);
      public long count();
      boolean deleteFamily(Family family);
     boolean saveFamily(List<Family> family);

    public boolean deleteFamilyByIndex(int index);
      public void bornChild(Family family, String name);

//
//    public Family deleteFamily(int index);
//
//    public Family deleteFamily(Family family);
//
//    public Family saveFamily(Family family);
//
//
//    public void displayAllFamilies();
//
//
//
//    public void countFamiliesWithMemberNumber (int number);
//    public void deleteFamilyByIndex (int index);

//    public void adoptChild (int number);
//    public void deleteAllChildrenOlderThen  (int number);
    public Family getFamilyById(int number);
    public List getPets(int number);

    public void addPet(int index, Pet pet);












}
