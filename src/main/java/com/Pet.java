
package com;

import java.util.ArrayList;

public class Pet {
    private Species species;
    String nickname;
    int age;
    int trickLevel;
    ArrayList<String> habits = new ArrayList<>();

    public void eat() {
        System.out.println("I am eating");

    }

    public Species getSpecies() {

        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public void respond() {
        System.out.println("Hello,owner.I am " + nickname + ".I miss you!");
    }

    public void foul() {
        System.out.println("I need to cover it up");
    }


    @Override
    public String toString() {
        return "Pet{" +
                "species=" + species +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits +
                '}';
    }

    public Pet() {
    }

    public Pet(String nickname) {
        this.nickname = nickname;
    }

    public Pet(Species species, String nickname, int age, int trickLevel, ArrayList habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
}


