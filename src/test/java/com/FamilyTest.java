package com;

import com.DAO.CollectionFamilyDao;
import com.Service.FamilyService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BooleanSupplier;

import static org.junit.jupiter.api.Assertions.*;


class FamilyTest {
    public Family family;
    public FamilyService familyService;
    public CollectionFamilyDao collectionFamilyDao;

    private Human father;
     private Human mother;
    private Human deleted_human;
    private Human added_human;

    @BeforeEach
    void familyInfo() {
        mother = new Human("Rafig", "Lahijov", 2002);
        father = new Human("Samir", "Lahijov", 1891);
        family= new Family(mother, father);


//        Human mother2 = new Human("sc", "Lahijqqov", 2002);
//       Human father2 = new Human("za", "ee", 1891);
//        family = new Family(mother2, father2);


        Pet emilioPet = new Pet(Species.CAT, "jane", 21, 8, new ArrayList(3));
        family.setPet(emilioPet);
        deleted_human = new Human("deleted_human_1", "Deleted", 1882);


ArrayList<Family>lis=new ArrayList<>();
lis.add(family);
        Human added_human2 = new Human("sad", "d", 1252);

        added_human = new Human("added_human_1", "added", 1252);
        familyService.saveFamily(lis);

     }

    @Test
    void testToString() {
        System.out.println(family.getMother());
        String expected = "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + family.children +
                ", pet=" + family.getPet() +
                '}';
        Assertions.assertEquals(family.toString(), expected);

    }

    @Test
    void addChild() {
        family.addChild(added_human);

        assertTrue(((family.children).contains((added_human))));
    }

    @Test
    void deleteChild() {
        family.addChild(added_human);

        assertTrue(family.deleteChild(0));
        assertFalse(family.children.contains(deleted_human));
    }


    @Test
    void deleteNonExistingChild() {
        assertFalse(family.deleteChild(10));
    }

    @Test
    void countFamily() {
        Assertions.assertEquals(4, family.countFamily());
    }

    @Test
    void getAllFamilies() {
        List<Family>fams=new ArrayList<>();
        family= new Family(mother, father);
        for ( Family family:fams) {

             fams.add(family);
        }

        Assertions.assertEquals(fams,familyService.getAllFamilies());

    }



    @Test
    void getFamiliesBiggerThan(int index) {

    }

    @Test
    void getFamiliesLessThan() {
    }

    @Test
    void createNewFamily() {
    }


    @Test
    void saveFamily( ) {
        mother = new Human("Rafig", "Lahijov", 2002);
        father = new Human("Samir", "Lahijov", 1891);
        family= new Family(mother, father);

        List<Family>familyList=new ArrayList<>();

        familyList.add(family);
        collectionFamilyDao.saveFamily(familyList);
        assertFalse(familyList.isEmpty());

    }

    @Test
    void deleteFamilyByIndex() {
    }

    @Test
    void bornChild() {
    }



    @Test
    void getPets() {
    }

    @Test
    void addPet() {
    }
}
